import { ThirdPartyAppValue } from '../shared/enums/ThirdPartyAppValue.enum';
import { ThirdPartyApp } from '../shared/interfaces/ThirdPartyApp.interface';

export const thirdPartyApps: ThirdPartyApp[] = [
    {
        value: ThirdPartyAppValue.STRAVA,
        name: 'Strava',
        domain: 'https://www.strava.com',
        auth_path: '/oauth/authorize',
        access_token_path: 'https://www.strava.com/oauth/token',
        redirect_uri: `${window.location.origin}/callback`,
        client_id: '37670',
        iconPath: './assets/images/strava-icon.png',
        scope: 'read,activity:read_all,profile:read_all',
        client_secret: '5691f0c818f74be32c0d3f7fdc911cd7268dd453', // Not secure, in production store this data (and well other) on server. 
        // Since Implicit Grant Flow is not recomended, we will simulate with Authorization Code Flow with client_secret on frontend
    },
    {
        value: ThirdPartyAppValue.FITBIT,
        name: 'FitBit',
        domain: 'https://www.fitbit.com',
        auth_path: '/oauth2/authorize',
        access_token_path: 'https://api.fitbit.com/oauth2/token',
        redirect_uri: `${window.location.origin}/callback`,
        client_id: '22B6BQ',
        iconPath: './assets/images/fitbit-icon.png',
        scope: 'activity%20heartrate%20location%20nutrition%20profile%20settings%20sleep%20social%20weight',
        client_secret: '70db32da88268ea50849087c0c3b1af8',
    }
];

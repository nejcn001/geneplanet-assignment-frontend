export const oAuth2 = {
    response_type: 'code',
    redirect_uri: `${window.location.origin}/oauth/authorize`,
    approval_prompt: 'force'
};

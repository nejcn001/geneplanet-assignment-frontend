import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { ThirdPartyAppAuthComponent } from './core/authentication/components/thirdPartyAppAuth/thirdPartyAppAuth.component';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { HomeModule } from './modules/home/home.module';
import { DashboardModule } from './modules/dashboard/dashboard.module';
import { MenubarModule } from 'primeng/menubar';
import { TranslateModule } from '@ngx-translate/core';
import { PageNotFoundComponent } from './modules/page-not-found/page-not-found.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserModule,
        CommonModule,
        HttpClientModule,
        AppRoutingModule,
        HomeModule,
        DashboardModule,
        MenubarModule,
        TranslateModule.forRoot({})
      ],
      declarations: [
        AppComponent,
        ThirdPartyAppAuthComponent,
        PageNotFoundComponent,

      ],
    }).compileComponents();
  }));

  it('should create the AppComponent', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`Should create menu bar with 2 options'`, async () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance as AppComponent;
    await app.ngOnInit();
    expect(app.menuItems.length).toEqual(2);
  });

});

import { Component, OnInit } from '@angular/core';
import { StravaService } from 'src/app/core/services/thirdPartyServices/strava/strava.service';
import { StravaAthlete } from 'src/app/shared/interfaces/StravaAthlete.interface';
import { StravaActivity } from 'src/app/shared/interfaces/StravaActivity.interface';
import { FitbitService } from 'src/app/core/services/thirdPartyServices/fitbit/fitbit.service';
import { ThirdPartyAppValue } from 'src/app/shared/enums/ThirdPartyAppValue.enum';
import { LocalStorageService } from 'src/app/core/services/localStorage.service';
import { FitbitProfile } from 'src/app/shared/interfaces/FitbitProfile.interface';
import { FitbitWeight } from 'src/app/shared/interfaces/FitbitWeight.interface';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    stravaAthlete: StravaAthlete;
    stravaActivities: StravaActivity[];
    fitbitProfile: FitbitProfile;
    fitbitWeightHistory: FitbitWeight[];

    weightBodyFatChart;

    constructor(private stravaService: StravaService, private fitbitService: FitbitService, private localStorageService: LocalStorageService, private translate: TranslateService) {

        this.weightBodyFatChart = {
            labels: [],
            datasets: [
                {
                    label: this.translate.instant('weight'),
                    data: []
                },
                {
                    label: this.translate.instant('body-fat') + '%',
                    data: []
                }
            ]
        };

        this.stravaService.dataSynced$.subscribe((response: any) => {
            this.stravaAthlete = response.stravaAthlete;
            this.stravaActivities = response.stravaActivities;
        });
        this.fitbitService.dataSynced$.subscribe((response: any) => {
            this.fitbitProfile = response.fitbitProfile.user;
            this.fitbitWeightHistory = response.fitbitWeightHistory.weight;
            for (const weightLog of this.fitbitWeightHistory) {
                this.weightBodyFatChart.labels.push(weightLog.date);
                this.weightBodyFatChart.datasets[0].data.push(weightLog.weight);
                this.weightBodyFatChart.datasets[1].data.push(weightLog.fat);
            }
        });
    }

    // Read app data and set sync interval on opening dashboard page
    async ngOnInit() {
        if (this.localStorageService.getThirdPartyAuthData(ThirdPartyAppValue.STRAVA) != null) {
            this.stravaService.startSyncInterval();
        }
        if (this.localStorageService.getThirdPartyAuthData(ThirdPartyAppValue.FITBIT) != null) {
            const oneMonthAgo = new Date();
            oneMonthAgo.setMonth(oneMonthAgo.getMonth() - 1);
            this.fitbitService.startSyncInterval(oneMonthAgo, new Date());
        }
    }

}

import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { TableModule } from 'primeng/table';
import { ChartModule } from 'primeng/chart';
import { DashboardComponent } from './dashboard.component';
import { HttpClientModule } from '@angular/common/http';

describe('DashboardComponent', () => {

    let comp: DashboardComponent;
    let fixture: ComponentFixture<DashboardComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule, TableModule, TranslateModule.forRoot(), ChartModule, HttpClientModule
            ],
            declarations: [
                DashboardComponent
            ],
            providers: []
        }).compileComponents().then(() => {
            fixture = TestBed.createComponent(DashboardComponent);
            comp = fixture.componentInstance;
        });
    }));

    it('should create the DashboardComponent', () => {
        expect(comp).toBeTruthy();
    });

    it('Before strava data is read, shows no data text', async () => {
        fixture.detectChanges();
        await fixture.whenStable();
        expect(fixture.nativeElement.querySelector('#no-strava-data').innerHTML).toEqual('no-data-connect-to-app.strava');
    });

    it('Before fitbit data is read, shows no data text', async () => {
        fixture.detectChanges();
        await fixture.whenStable();
        expect(fixture.nativeElement.querySelector('#no-fitbit-data').innerHTML).toEqual('no-data-connect-to-app.fitbit');
    });

});

import { Component } from '@angular/core';
import { thirdPartyApps } from 'src/app/configs/thirdPartyApps.config';
import { ThirdPartyApp } from 'src/app/shared/interfaces/ThirdPartyApp.interface';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: [ './home.component.scss']
})
export class HomeComponent {

    thirdPartyApps: ThirdPartyApp[] = thirdPartyApps;

    constructor() {}

}

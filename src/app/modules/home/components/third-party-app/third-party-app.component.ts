import { Component, Input } from '@angular/core';
import { ThirdPartyApp } from 'src/app/shared/interfaces/ThirdPartyApp.interface';
import { oAuth2 } from 'src/app/configs/oauth2.config';
import { LocalStorageService } from 'src/app/core/services/localStorage.service';
import { StravaService } from 'src/app/core/services/thirdPartyServices/strava/strava.service';

@Component({
    selector: 'app-third-party-app',
    templateUrl: './third-party-app.component.html',
    styleUrls: ['./third-party-app.component.scss']
})
export class ThirdPartyAppComponent {

    @Input() thirdPartyApp: ThirdPartyApp;

    constructor(private localStorageService: LocalStorageService) { }

    connect() {
        const url = `${this.thirdPartyApp.domain}${this.thirdPartyApp.auth_path}?client_id=${this.thirdPartyApp.client_id}` +
            `&response_type=${oAuth2.response_type}` +
            `&redirect_uri=${this.thirdPartyApp.redirect_uri}` +
            `&approval_prompt=${oAuth2.approval_prompt}` +
            `&state=${this.thirdPartyApp.value}` +
            `&scope=${this.thirdPartyApp.scope}`;
        window.open(url);
    }

    disconnect() {
        localStorage.removeItem(this.thirdPartyApp.value);
    }

    isConnectedToApp(): boolean {
        const appAuthData = this.localStorageService.getThirdPartyAuthData(this.thirdPartyApp.value);
        if (appAuthData == null || appAuthData.access_token == null) { return false; }
        const authExpiredDate = new Date(appAuthData.expires_at * 1000);
        return (authExpiredDate > new Date());
    }

}

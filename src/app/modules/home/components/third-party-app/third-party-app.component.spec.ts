import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { ThirdPartyAppComponent } from './third-party-app.component';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { TranslateModule } from '@ngx-translate/core';
import { thirdPartyApps } from 'src/app/configs/thirdPartyApps.config';
import { HttpClientModule } from '@angular/common/http';

describe('ThirdPartyAppComponent', () => {

    let comp: ThirdPartyAppComponent;
    let fixture: ComponentFixture<ThirdPartyAppComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                ButtonModule,
                TranslateModule,
                HttpClientModule
            ],
            declarations: [
                ThirdPartyAppComponent
            ]
        }).compileComponents().then(() => {
            fixture = TestBed.createComponent(ThirdPartyAppComponent);
            comp = fixture.componentInstance;
        });
    }));

    it('should create the ThirdPartyAppComponent', () => {
        expect(comp).toBeTruthy();
    });

    it(`After disconnect, app shouldnt be connected`, async () => {
        comp.thirdPartyApp = thirdPartyApps[0]; // sets strava config
        comp.disconnect();
        expect(comp.isConnectedToApp()).toBeFalsy();
    });

});

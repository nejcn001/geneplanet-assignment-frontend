import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { ButtonModule } from 'primeng/button';
import { TranslateModule } from '@ngx-translate/core';
import { ThirdPartyAppComponent } from './components/third-party-app/third-party-app.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [HomeComponent, ThirdPartyAppComponent],
  imports: [CommonModule, ButtonModule, TranslateModule],
  providers: [],
})
export class HomeModule { }

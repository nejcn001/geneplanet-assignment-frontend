import { TestBed, async } from '@angular/core/testing';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { HomeComponent } from './home.component';
import { ThirdPartyAppComponent } from './components/third-party-app/third-party-app.component';
import { ButtonModule } from 'primeng/button';

describe('HomeComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        ButtonModule,
        TranslateModule,
      ],
      declarations: [
        HomeComponent,
        ThirdPartyAppComponent,
      ],
    }).compileComponents();
  }));

  it('should create the HomeComponent', () => {
    const fixture = TestBed.createComponent(HomeComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`Should have at least one third party app`, async () => {
    const fixture = TestBed.createComponent(HomeComponent);
    const app = fixture.debugElement.componentInstance as HomeComponent;
    await app.thirdPartyApps;
    expect(app.thirdPartyApps.length).toBeGreaterThan(0);
  });

});

import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/components/common/menuitem';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    menuItems: MenuItem[];

    constructor(private translate: TranslateService, private router: Router) {
        translate.setDefaultLang('sl');
    }

    async ngOnInit() {
        await this.translate.use('sl').toPromise();
        this.menuItems = [
            {
                label: this.translate.instant('home'),
                icon: 'pi pi-home',
                command: (event: any) => this.router.navigateByUrl('/')
            },
            {
                label: this.translate.instant('dashboard'),
                icon: 'pi pi-chart-bar',
                command: (event: any) => this.router.navigateByUrl('/dashboard')
            }
        ];
    }
}

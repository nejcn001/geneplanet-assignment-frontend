import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LocalStorageService } from '../../localStorage.service';
import { ThirdPartyAppValue } from 'src/app/shared/enums/ThirdPartyAppValue.enum';
import { ThirdPartyApp } from 'src/app/shared/interfaces/ThirdPartyApp.interface';
import { StravaAthlete } from 'src/app/shared/interfaces/StravaAthlete.interface';
import { StravaActivity } from 'src/app/shared/interfaces/StravaActivity.interface';
import { interval, Subject } from 'rxjs';
import { startWith } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class StravaService {

  stravaAthlete: StravaAthlete;
  stravaActivities: StravaActivity[];
  syncInterval;

  private dataSynced = new Subject();
  dataSynced$ = this.dataSynced.asObservable();

  constructor(private http: HttpClient, private localStorageService: LocalStorageService) { }

  getOAuthToken(thirdPartyApp: ThirdPartyApp, authCode: string): Promise<any> {
    const data = {
      client_id: thirdPartyApp.client_id,
      client_secret: thirdPartyApp.client_secret, // For simulation purposes, in production, add this on server
      code: authCode,
      grant_type: 'authorization_code'
    };

    return this.http.post(`${thirdPartyApp.access_token_path}`, data).toPromise();
  }

  refreshAccessToken(thirdPartyApp: ThirdPartyApp, refreshToken: string): Promise<any> {
    const data = {
      client_id: thirdPartyApp.client_id,
      client_secret: thirdPartyApp.client_secret, // For simulation purposes, in production, add this on server
      refresh_token: refreshToken,
      grant_type: 'refresh_token'
    };

    return this.http.post(`${thirdPartyApp.access_token_path}`, data).toPromise();
  }

  startSyncInterval() {
    if (this.syncInterval == null) {
      this.syncInterval = interval(1000 * 60 * 60).pipe(
        startWith(0),
      ).subscribe(async () => {
        [this.stravaAthlete, this.stravaActivities] = await Promise.all([this.getAthlete(), this.getActivities()]);
        this.restributeSyncData();
      });
    } else {
      this.restributeSyncData();
    }
  }

  restributeSyncData() {
    this.dataSynced.next({ stravaAthlete: this.stravaAthlete, stravaActivities: this.stravaActivities });
  }

  constructHeaders() {
    const authAppData = this.localStorageService.getThirdPartyAuthData(ThirdPartyAppValue.STRAVA);
    const headers = {
      headers: new HttpHeaders().set('Authorization', `Bearer ${(authAppData != null) ? authAppData.access_token : null}`)
    };
    return headers;
  }

  getAthlete(): Promise<any> {
    return this.http.get('https://www.strava.com/api/v3/athlete', this.constructHeaders()).toPromise();
  }

  getActivities(): Promise<any> {
    return this.http.get('https://www.strava.com/api/v3/activities', this.constructHeaders()).toPromise();
  }

}

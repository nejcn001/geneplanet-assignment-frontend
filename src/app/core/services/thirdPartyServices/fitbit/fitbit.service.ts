import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LocalStorageService } from '../../localStorage.service';
import { ThirdPartyAppValue } from 'src/app/shared/enums/ThirdPartyAppValue.enum';
import { ThirdPartyApp } from 'src/app/shared/interfaces/ThirdPartyApp.interface';
import { Subject, interval } from 'rxjs';
import { startWith } from 'rxjs/operators';
import { FitbitProfile } from 'src/app/shared/interfaces/FitbitProfile.interface';
import { FitbitWeight } from 'src/app/shared/interfaces/FitbitWeight.interface';

@Injectable({ providedIn: 'root' })
export class FitbitService {

  fitbitProfile: FitbitProfile;
  fitbitWeightHistory: FitbitWeight[];
  syncInterval;
  private dataSynced = new Subject();
  dataSynced$ = this.dataSynced.asObservable();

  constructor(private http: HttpClient, private localStorageService: LocalStorageService) { }

  getOAuthToken(thirdPartyApp: ThirdPartyApp, authCode: string): Promise<any> {
    // For Authorization Code Flow simulation purposes, use client_secret on server only
    const authB64Encoded = btoa(`${thirdPartyApp.client_id}:${thirdPartyApp.client_secret}`);

    const headers = {
      headers: new HttpHeaders()
        .set('Authorization', `Basic ${authB64Encoded}`)
        .set('Content-Type', 'application/x-www-form-urlencoded')
    };
    const uri = `${thirdPartyApp.access_token_path}?client_id=${thirdPartyApp.client_id}&code=${authCode}&grant_type=authorization_code&redirect_uri=${thirdPartyApp.redirect_uri}`;
    return this.http.post(uri, null, headers).toPromise();
  }

  refreshAccessToken(thirdPartyApp: ThirdPartyApp, refreshToken: string): Promise<any> {
    // For Authorization Code Flow simulation purposes, use client_secret on server only
    const authB64Encoded = btoa(`${thirdPartyApp.client_id}:${thirdPartyApp.client_secret}`);

    const headers = {
      headers: new HttpHeaders()
        .set('Authorization', `Basic ${authB64Encoded}`)
        .set('Content-Type', 'application/x-www-form-urlencoded')
    };
    const uri = `${thirdPartyApp.access_token_path}?refresh_token=${refreshToken}&grant_type=refresh_token`;
    return this.http.post(uri, null, headers).toPromise();
  }

  startSyncInterval(dateFrom: Date, dateTo: Date) {
    if (this.syncInterval == null) {
      this.syncInterval = interval(1000 * 60 * 60).pipe(
        startWith(0),
      ).subscribe(async () => {
        [this.fitbitProfile, this.fitbitWeightHistory] = await Promise.all([
          this.getProfile(),
          this.getWeightHistory(dateFrom, dateTo)]);
        this.restributeSyncData();
      });
    } else {
      this.restributeSyncData();
    }
  }

  restributeSyncData() {
    this.dataSynced.next({ fitbitProfile: this.fitbitProfile, fitbitWeightHistory: this.fitbitWeightHistory });
  }

  constructHeaders() {
    const authAppData = this.localStorageService.getThirdPartyAuthData(ThirdPartyAppValue.FITBIT);
    const headers = {
      headers: new HttpHeaders().set('Authorization', `Bearer ${(authAppData != null) ? authAppData.access_token : null}`)
    };
    return headers;
  }

  constructFormatDate(date: Date): string {
    return `${date.getUTCFullYear()}-${String(date.getUTCMonth() + 1).padStart(2, '0')}-${String(date.getDate()).padStart(2, '0')}`;
  }

  getProfile(): Promise<any> {
    return this.http.get('https://api.fitbit.com/1/user/-/profile.json', this.constructHeaders()).toPromise();
  }

  getWeightHistory(dateFrom: Date, dateTo: Date): Promise<any> {
    const dateFromString = this.constructFormatDate(dateFrom);
    const dateToString = this.constructFormatDate(dateTo);
    return this.http.get(`https://api.fitbit.com/1/user/-/body/log/weight/date/${dateFromString}/${dateToString}.json`, this.constructHeaders()).toPromise();
  }

}

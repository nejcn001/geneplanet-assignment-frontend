import { Injectable } from '@angular/core';
import { ThirdPartyAppValue } from 'src/app/shared/enums/ThirdPartyAppValue.enum';

@Injectable({ providedIn: 'root' })
export class LocalStorageService {

  getThirdPartyAuthData(value: ThirdPartyAppValue): any {
    return JSON.parse(localStorage.getItem(value));
  }

}

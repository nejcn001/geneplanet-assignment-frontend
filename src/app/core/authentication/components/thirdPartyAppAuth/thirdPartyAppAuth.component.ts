import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ThirdPartyAppValue } from 'src/app/shared/enums/ThirdPartyAppValue.enum';
import { thirdPartyApps } from 'src/app/configs/thirdPartyApps.config';
import { StravaService } from 'src/app/core/services/thirdPartyServices/strava/strava.service';
import { FitbitService } from 'src/app/core/services/thirdPartyServices/fitbit/fitbit.service';

@Component({
    selector: 'app-third-party-app-auth',
    template: '',
})
export class ThirdPartyAppAuthComponent implements OnInit {

    constructor(private route: ActivatedRoute, private router: Router, private stravaService: StravaService, private fitbitService: FitbitService) { }

    async ngOnInit() {
        if (this.route.snapshot.queryParamMap.get('error') == null) {
            const code: string = this.route.snapshot.queryParamMap.get('code');
            const thirdPartyAppValue = this.route.snapshot.queryParamMap.get('state') as ThirdPartyAppValue;
            const thirdPartyApp = thirdPartyApps.find(app => app.value === thirdPartyAppValue);
            let response;
            let localStorageData;
            switch (thirdPartyAppValue) {
                case ThirdPartyAppValue.STRAVA:
                    response = await this.stravaService.getOAuthToken(thirdPartyApp, code); // https://developers.strava.com/docs/authentication/
                    localStorageData = { code, access_token: response.access_token, refresh_token: response.refresh_token, expires_at: response.expires_at, user_id: response.athlete.id };
                    break;
                case ThirdPartyAppValue.FITBIT:
                    response = await this.fitbitService.getOAuthToken(thirdPartyApp, code); // https://dev.fitbit.com/build/reference/web-api/oauth2/
                    const expiresAt = Math.floor(Date.now() / 1000) + response.expires_in;
                    localStorageData = { code, access_token: response.access_token, refresh_token: response.refresh_token, expires_at: expiresAt, user_id: response.user_id};
                    break;
                default: this.router.navigateByUrl('');
            }
            localStorage.setItem(thirdPartyAppValue, JSON.stringify(localStorageData));
        }
        this.router.navigateByUrl('');
    }

}

import { ThirdPartyAppValue } from '../enums/ThirdPartyAppValue.enum';

export interface ThirdPartyApp {
    value: ThirdPartyAppValue;
    name: string;
    domain: string;
    auth_path: string;
    access_token_path: string;
    redirect_uri: string;
    client_id: string;
    iconPath: string;
    scope: string;
    client_secret: string;
}

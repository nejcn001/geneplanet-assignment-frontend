export interface StravaAthlete {
    username?: any;
    resource_state: number;
    firstname: string;
    lastname: string;
    city?: any;
    state?: any;
    country?: any;
    sex: string;
    premium: boolean;
    summit: boolean;
    created_at: Date;
    updated_at: Date;
    badge_type_id: number;
    profile: string;
    friend?: any;
    follower?: any;
}

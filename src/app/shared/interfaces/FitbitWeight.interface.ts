export interface FitbitWeight {
    bmi: number;
    date: string;
    fat: number;
    logId: any;
    source: string;
    time: string;
    weight: number;
}
export enum ThirdPartyAppValue {
    STRAVA = 'strava',
    FITBIT = 'fitbit'
}

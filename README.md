Assignment Info
The purpose of the challenge is to build a working version of All in One Health tracker.

List of possible 3rd party services is available below, however, feel free to use an alternative. The only requirement is that the metrics are related to health / sport domains.

The service represents your level of expertise and we trust that you will solve it yourself. You should be able to explain your decisions that you have made while planning and developing the service.

The assignment should not take you more than 3 days.

Your quest
-	Implement two tabs
o	Home screen where links to all 3rd party services as available
	Each 3rd party service should have a “Connect” link which establishes a secured channel to collect metrics
o	Dashboard screen where aggregated data from all 3rd party services is presented to the end user. 
-	At least one 3rd party service has to use OAuth 2 authorization
-	Get 5 metrics from each APIs and present them as icons (e.g. let the icon contain a text presentation for ratio comparison)
-	Metric sync is to be done on hourly basis
-	Write unit tests (code coverage at least at 80%)
-	Write integration tests
-	The project must have a VCS repository with enough revisions to see the work progress.

Architecture / Design
-	The application is to be designed with responsiveness in mind (can run on the desktop, tablet or mobile).

Documentation
-	Prepare a document that describes the solution and how it is used.

Languages
You can choose among the following programming languages: 
-	Frontend
o	Angular (preferred)
o	React 

3rd party services
List of service providers: 
-	Strava (https://developers.strava.com/) 
-	RunKeeper (https://runkeeper.com/index) 
-	Sony Lifelog (https://developer.sony.com/develop/services/lifelog-api/)
-	TomTom MySports (https://mysports.tomtom.com/)
-	Nutritics (https://www.nutritics.com/p/api) 
